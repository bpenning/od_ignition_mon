
from lz_ignition_dbi.lz_ignition_dbi import IDBIWrapper
import matplotlib.pyplot as plt
from matplotlib.ticker import Formatter    # to convert xaxis label to dates
from datetime import datetime, timedelta
import pytz
import pandas as pd

TIMEZONE = 'US/Mountain'
DATEFORMAT = '%Y-%-m-%-dT%H:%M'  # see https://strftime.org/

class DateFormat(Formatter):
    """
    This class converts the seconds since unix epoch to date strings
    """
    def __init__(self, format=None, timezone=None):
        self.format = format if format is not None else '%Y-%m-%dT%H:%M'
        self.timezone = timezone if timezone is not None else 'US/Mountain'

    def __call__(self, x, pos=None):
        tz = pytz.timezone(TIMEZONE)
        newticklabel = datetime.fromtimestamp(x).astimezone(tz).strftime(self.format)
        return newticklabel


dateformatter = DateFormat(DATEFORMAT, TIMEZONE)

if __name__ == '__main__':

    df = pd.DataFrame()
    df_created=False
    
    def plotIgnition(fname,ylabel,ax=None,options=''):
        print('Retrieving '+fname)
        if ax is None:
            ax = plt.gca()
        
        tags = [fname]
        

        idbi = IDBIWrapper(known_tags_file='/global/u2/p/penning/work/od_igniton_mon/bp_tag_map_PMTs.json')
        idbi.select(tags, start, end)
        data = idbi.resolve(dateformat='unix')

        # DBI returns times in units of ms. Convert to s for formatter
        data[fname]['times'] = [x / 1000. for x in data[fname]['times']]
        # Make the plot
        fig = plt.figure()
        ax.plot(data[fname]['times'], data[fname]['vals'], options, label=fname)
        plt.title(fname)
        plt.ylabel(ylabel)
        ax.legend(frameon=False)

        # Convert UNIX timestamps to human-readable
        ax.xaxis.set_major_formatter(dateformatter)

        # Rotate the tick labels and align them to the right
        for tick in ax.xaxis.get_majorticklabels():
            tick.set_horizontalalignment('right')
            tick.set_rotation(30)
        fig.subplots_adjust(bottom=0.2, left=0.15)
        plt.show()
        return ax

    
    ###
    ###  Start here main code
    ###

    #select start and ened times
#    start = '2021-01-01T12:00:00'  # MT
#    end = '2023-01-16T12:00:00'  # MT
    end = datetime.today()#.strftime('%Y-%m-%d %H:%M:%S')
    start= datetime.today() - timedelta(days=4)
 
   
    
    f, (ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9, ax10, ax11, ax12, ax13) = plt.subplots(13, 1, sharex=True, figsize=(12,35), dpi=100)
    plotIgnition('5505OHM', 'Resistivity [MOhm/cm]', ax1, 'b')
    plotIgnition('5502DO', 'DO2 [ppb]', ax2, 'r')
    plotIgnition('RAD73', 'RAD [cts]', ax3, 'darkorange')
    plotIgnition('DAVISP', 'p [mbar]', ax4,  'peru')
    plotIgnition('DAVIST', 't [C]', ax5,  'salmon')
    plotIgnition('6405FM', '6405FM', ax6,  'powderblue')
    plotIgnition('6401FM', '6401FM', ax7,  'cadetblue')
    plotIgnition('6233PT', '6233PT', ax8,  'lime')
    plotIgnition('6232PT', '6232PT', ax9,  'limegreen')
    plotIgnition('6231PT', '6231PT', ax10,  'seagreen')
    plotIgnition('6405RG', '6405RG', ax11,  'darkslategrey')
    plotIgnition('S1ODROW1', 'S1ODROW1', ax12,  'steelblue')
    plotIgnition('S1ODROW2', 'S1ODROW2', ax12,  'lightslategrey')
    plotIgnition('S1ODROW3', 'S1ODROW3', ax12,  'slateblue')
    plotIgnition('S1ODROW4', 'S1ODROW4', ax12,  'darkslateblue')
    plotIgnition('S1ODROW5', 'S1ODROW5', ax12,  'midnightblue')
    plotIgnition('S1ODROW6', 'S1ODROW6', ax12,  'lightblue')
    plotIgnition('OD1Temp', 'OD1Temp', ax13,  'maroon')
    plotIgnition('OD3Temp', 'OD2Temp', ax13,  'lightcoral')
    plotIgnition('OD2Temp', 'OD3Temp', ax13,  'peachpuff')


    f.savefig('monitor.png')
 
